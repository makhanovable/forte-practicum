package kz.forte.app.practicum.di

import com.google.gson.Gson
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

val apiModule = module {

    single<OkHttpClient> {

        OkHttpClient.Builder()
            .retryOnConnectionFailure(true)
            .connectTimeout(60_000L, TimeUnit.MILLISECONDS)
            .readTimeout(60_000L, TimeUnit.MILLISECONDS)
            .build()
    }


    single<Retrofit.Builder> {
        val gson: Gson = Gson().newBuilder().create()
        Retrofit.Builder()
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    single<Retrofit> {
        val retrofitBuilder: Retrofit.Builder = get()
        val client: OkHttpClient = get()
        return@single retrofitBuilder
            .baseUrl("https://qa-anthill-api.fortebank.com/v1/")
            .client(client)
            .build()
    }
}