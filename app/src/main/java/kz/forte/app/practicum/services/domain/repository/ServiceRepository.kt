package kz.forte.app.practicum.services.domain.repository

import kz.forte.app.practicum.base.Either
import kz.forte.app.practicum.services.domain.models.ServiceListModel

interface ServiceRepository {

    suspend fun getServices(): Either<Throwable, ServiceListModel>
}