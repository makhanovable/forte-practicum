package kz.forte.app.practicum.base

sealed class Either<out L, out R> {
    data class Left<out L>(val error: L) : Either<L, Nothing>()
    data class Right<out R>(val data: R) : Either<Nothing, R>()
}