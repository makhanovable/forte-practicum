package kz.forte.app.practicum.services.domain.usecases

import kz.forte.app.practicum.base.Either
import kz.forte.app.practicum.services.domain.models.ServiceListModel
import kz.forte.app.practicum.services.domain.repository.ServiceRepository

class GetServiceListUseCase(
    private val serviceRepository: ServiceRepository
) {

    suspend operator fun invoke(): Either<Throwable, ServiceListModel> {
        return serviceRepository.getServices()
    }
}