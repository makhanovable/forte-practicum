package kz.forte.app.practicum.services.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import kz.forte.app.practicum.databinding.FragmentServicesBinding
import kz.forte.app.practicum.services.domain.usecases.GetServiceListUseCase
import org.koin.androidx.viewmodel.ext.android.viewModel

class ServicesFragment : Fragment() {
    private lateinit var viewBinding: FragmentServicesBinding
    private val viewModel: ServicesViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        viewBinding = FragmentServicesBinding.inflate(inflater, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeLiveData()

        viewModel.loadServices()
    }

    private fun observeLiveData() {
        viewModel.getServicesLiveData().observe(viewLifecycleOwner) { serviceListModel ->
            val adapter: ServiceAdapter = ServiceAdapter(serviceListModel.serviceList)
            viewBinding.recyclerView.adapter = adapter
        }
        viewModel.getErrorLiveData().observe(viewLifecycleOwner) { error ->
            Toast.makeText(requireContext(), error, Toast.LENGTH_LONG).show()
        }
    }
}