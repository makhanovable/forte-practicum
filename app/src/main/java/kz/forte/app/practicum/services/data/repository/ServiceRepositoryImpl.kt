package kz.forte.app.practicum.services.data.repository

import kz.forte.app.practicum.base.BaseRepository
import kz.forte.app.practicum.base.Either
import kz.forte.app.practicum.services.data.ServiceApi
import kz.forte.app.practicum.services.data.mapper.toServiceListModel
import kz.forte.app.practicum.services.domain.models.ServiceListModel
import kz.forte.app.practicum.services.domain.repository.ServiceRepository

class ServiceRepositoryImpl(
    private val serviceApi: ServiceApi
) : ServiceRepository, BaseRepository() {

    override suspend fun getServices(): Either<Throwable, ServiceListModel> {
        return apiCall {
            serviceApi
                .getServiceList()
                .toServiceListModel()
        }
    }
}