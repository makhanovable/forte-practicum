package kz.forte.app.practicum.services.data.models

import com.google.gson.annotations.SerializedName

data class ServiceListResponse(
    @SerializedName("services") val serviceList: List<ServiceResponse>?
)

data class ServiceResponse(
    @SerializedName("name") val title: String?,
    @SerializedName("imgLink") val imageLink: String?
)