package kz.forte.app.practicum.base

import java.lang.Exception

class BadDataResponseException : Exception()