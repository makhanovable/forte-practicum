package kz.forte.app.practicum.services.domain.models

data class ServiceListModel(
    val serviceList: List<ServiceModel>
)

data class ServiceModel(
    val title: String,
    val imageLink: String
)