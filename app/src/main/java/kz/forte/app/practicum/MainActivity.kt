package kz.forte.app.practicum

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kz.forte.app.practicum.databinding.ActivityMainBinding
import kz.forte.app.practicum.utils.viewBinding
import kz.forte.app.practicum.services.presentation.ServicesFragment

class MainActivity : AppCompatActivity() {

    private val viewBinding: ActivityMainBinding by viewBinding(ActivityMainBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(viewBinding.root)
        supportFragmentManager
            .beginTransaction()
            .add(R.id.frameContainer, ServicesFragment())
            .commit()
    }
}