package kz.forte.app.practicum.base

open class BaseRepository {

    protected suspend fun <T : Any> apiCall(call: suspend () -> T): Either<Throwable, T> =
        try {
            Either.Right(call.invoke())
        } catch (throwable: Throwable) {
            Either.Left(throwable)
        }
}