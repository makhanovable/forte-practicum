package kz.forte.app.practicum.di

import kz.forte.app.practicum.services.data.ServiceApi
import kz.forte.app.practicum.services.data.repository.ServiceRepositoryImpl
import kz.forte.app.practicum.services.domain.repository.ServiceRepository
import kz.forte.app.practicum.services.domain.usecases.GetServiceListUseCase
import kz.forte.app.practicum.services.presentation.ServicesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val serviceModules = module {

    single<ServiceApi> {
        val retrofit: Retrofit = get()
        return@single retrofit.create()
    }

    single<ServiceRepository> {
        ServiceRepositoryImpl(get())
    }


    factory {
        GetServiceListUseCase(get())
    }

    viewModel {
        ServicesViewModel(get())
    }
}

inline fun <reified Api> Retrofit.create(): Api = create(Api::class.java)