package kz.forte.app.practicum.services.data

import kz.forte.app.practicum.services.data.models.ServiceListResponse
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ServiceApi {

    @GET("services/all")
    suspend fun getServiceList(): ServiceListResponse
}