package kz.forte.app.practicum.services.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import kz.forte.app.practicum.base.Either
import kz.forte.app.practicum.services.domain.models.ServiceListModel
import kz.forte.app.practicum.services.domain.usecases.GetServiceListUseCase

class ServicesViewModel(
    private val getServiceListUseCase: GetServiceListUseCase
) : ViewModel() {

    private val servicesLiveData = MutableLiveData<ServiceListModel>()
    private val errorLiveData = MutableLiveData<String>()

    fun getServicesLiveData(): LiveData<ServiceListModel> = servicesLiveData
    fun getErrorLiveData(): LiveData<String> = errorLiveData

    fun loadServices() {
        viewModelScope.launch {
            when (val response = getServiceListUseCase()) {
                is Either.Right -> servicesLiveData.postValue(response.data)
                is Either.Left -> errorLiveData.postValue(response.error.toString())
            }
        }
    }
}