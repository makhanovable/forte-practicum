package kz.forte.app.practicum.services.data.mapper

import kz.forte.app.practicum.base.BadDataResponseException
import kz.forte.app.practicum.services.data.models.ServiceListResponse
import kz.forte.app.practicum.services.data.models.ServiceResponse
import kz.forte.app.practicum.services.domain.models.ServiceListModel
import kz.forte.app.practicum.services.domain.models.ServiceModel


fun ServiceListResponse.toServiceListModel(): ServiceListModel = ServiceListModel(
    serviceList = serviceList?.map {
        it.toServiceModel()
    } ?: throw BadDataResponseException()
)

fun ServiceResponse.toServiceModel(): ServiceModel = ServiceModel(
    title = title ?: "",
    imageLink = imageLink ?: ""
)