package kz.forte.app.practicum

import android.app.Application
import kz.forte.app.practicum.di.apiModule
import kz.forte.app.practicum.di.serviceModules
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class FortePracticumApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@FortePracticumApp)
            androidLogger(Level.ERROR)
            modules(
                listOf(
                    apiModule,
                    serviceModules,
                )
            )
        }
    }
}