package kz.forte.app.practicum.services.presentation

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kz.forte.app.practicum.databinding.ItemServiceBinding
import kz.forte.app.practicum.services.domain.models.ServiceModel


class ServiceAdapter(private val data: List<ServiceModel>): RecyclerView.Adapter<ServiceAdapter.MyViewHolder>() {
    inner class MyViewHolder(val itemBinding: ItemServiceBinding)
        : RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(properties: ServiceModel) {
            itemBinding.name.text = properties.title
            Glide.with(itemView).load(properties.imageLink).centerCrop().into(itemBinding.imageView)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(ItemServiceBinding.inflate(LayoutInflater.from(parent.context), parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(data[position])
    }
}